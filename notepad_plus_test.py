from pywinauto.application import Application
# from statesmachine import path_import_plugin
from tree_menu import TreeNotepadPlusMenu


def notepad_plus_test():
    # path_to_menuitem = path_import_plugin()
    # app.Notepad.menu_select(path_to_menuitem)
    tree = TreeNotepadPlusMenu()
    path_to_menuitem = tree.select_node('Импортировать плагин')
    app = Application().start('C:/Program Files/Notepad++/notepad++.exe')
    app.Notepad.menu_select(path_to_menuitem)

    app.kill()


if __name__ == '__main__':
    notepad_plus_test()
