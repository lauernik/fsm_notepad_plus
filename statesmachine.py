from transitions import Machine


class NotepadMachine:

    states = [
        'window', 'option_item', 'import_item', 'import_plugin_item'
    ]  # Задаём состояния

    transitions = [
        {'trigger': 'click_option', 'source': 'window', 'dest': 'option_item'},
        {'trigger': 'click_import', 'source': 'option_item', 'dest': 'import_item'},
        {'trigger': 'click_import_item', 'source': 'import_item', 'dest': 'import_plugin_item'},
        {'trigger': 'to_window', 'source': '*', 'dest': 'window'}
    ]  # Задаём переходы

    def __init__(self):
        self.machine = Machine(
            model=self, states=NotepadMachine.states, initial='window', transitions=NotepadMachine.transitions
        )

        self.path = ''  # инит пути до кнопки

    def on_enter_option_item(self): self.path += 'Опции -> '

    def on_enter_import_item(self): self.path += 'Импортировать -> '

    def on_enter_import_plugin_item(self):
        self.path += 'Импортировать плагин'
        # print(self.path)  # Передаём путь в pywinauto
        # self.to_window()  # Переходим в начальное состояние

    # def on_enter_window(self):
    #     self.path = ''


def path_import_plugin():
    notepad_plus = NotepadMachine()
    notepad_plus.click_option()
    notepad_plus.click_import()
    notepad_plus.click_import_item()
    return notepad_plus.path
