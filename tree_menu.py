from treelib import Tree


class ItemNotFound(Exception):
    pass


class TreeNotepadPlusMenu:

    def __init__(self):
        self.tree = Tree()
        self.create_tree()

    def create_tree(self):
        self.tree.create_node(identifier='root')

        # Root
        self.tree.create_node(identifier='файл', parent='root')
        self.tree.create_node(identifier='опции', parent='root')

        # Root -> файл
        self.tree.create_node(identifier='выход', parent='файл')

        # Root -> опции
        self.tree.create_node(identifier='настройки', parent='опции')
        self.tree.create_node(identifier='импортировать', parent='опции')

        # Root -> опции -> импортировать
        self.tree.create_node(identifier='импортировать плагин', parent='импортировать')

    def select_node(self, element):
        """
        Поиск пути в дереве для требуемого элемента
        :param element: *необходимо точное название
        :return: путь до элемента в формате x->y->z
        """
        paths = self.tree.paths_to_leaves()
        for path in paths:
            if element.lower() in path:
                return '->'.join(path[1:])
        raise ItemNotFound('Элемент не найден')
